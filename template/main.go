package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var reader *bufio.Scanner

//var cases int
var caseNumber int

func init() {
	reader = bufio.NewScanner(os.Stdin)
	reader.Scan()
	//cases, _ := strconv.Atoi(reader.Text())
	caseNumber = 1
}

func main() {
	for ; reader.Scan(); caseNumber++ {
		head, _ := strconv.Atoi(reader.Text())
		reader.Scan()
		values := lineToInts(reader.Text())
		Evaluate(head, values)
	}
}

//Evaluate is the main processing function
func Evaluate(head int, values []int) {
	result := 0

	output(result)
}

func output(values ...int) {
	//example
	//Case #1: 2 3
	temp := fmt.Sprintf("Case #%d:", caseNumber)
	for _, i := range values {
		temp += fmt.Sprintf(" %d", i)
	}
	fmt.Println(temp)
}

func lineToInts(line string) []int {
	fields := strings.Fields(line)
	ints := make([]int, len(fields))
	for i, val := range fields {
		j, _ := strconv.Atoi(val)
		ints[i] = j
	}
	return ints
}
